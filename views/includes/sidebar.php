<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Quick ERP</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item <?= $sidebarSection == 'Dashboard' ? 'active' : '' ?>">
        <a class="nav-link" href="<?= BASEPAGES ?>index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Interface
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item <?= $sidebarSection == 'category' ? 'active' : '' ?>">
        <a class="nav-link <?= $sidebarSection == 'category' ? '' : 'collapsed' ?>" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>Category</span>
        </a>
        <div id="collapseTwo" class="collapse <?= $sidebarSection == 'category' ? 'show' : '' ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item <?= $sidebarSubSection == 'add' && $sidebarSection == 'category' ? 'active' : '' ?>" href="<?= BASEPAGES ?>add-category.php">Add Category</a>
                <a class="collapse-item <?= $sidebarSubSection == 'manage' && $sidebarSection == 'category' ? 'active' : '' ?>" href="<?= BASEPAGES ?>manage-category.php">Manage Category</a>
            </div>
        </div>
    </li>


    <li class="nav-item <?= $sidebarSection == 'product' ? 'active' : '' ?>">
        <a class="nav-link <?= $sidebarSection == 'product' ? '' : 'collapsed' ?>" href="#" data-toggle="collapse" data-target="#collapseProduct" aria-expanded="true" aria-controls="collapseProduct">
            <i class="fas fa-fw fa-cog"></i>
            <span>Product</span>
        </a>
        <div id="collapseProduct" class="collapse <?= $sidebarSection == 'product' ? 'show' : '' ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item <?= $sidebarSubSection == 'add'  && $sidebarSection == 'product' ? 'active' : '' ?>" href="<?= BASEPAGES ?>add-product.php">Add product</a>
                <a class="collapse-item <?= $sidebarSubSection == 'manage' && $sidebarSection == 'product' ? 'active' : '' ?>" href="<?= BASEPAGES ?>manage-product.php">Manage product</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item <?= $sidebarSection == 'Customers' ? 'active' : '' ?>">
        <a class="nav-link <?= $sidebarSection == 'Customers' ? '' : 'collapsed' ?>" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Customers</span>
        </a>
        <div id="collapseUtilities" class="collapse <?= $sidebarSection == 'Customers' ? 'show' : '' ?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item <?= $sidebarSubSection == 'add-customers' ? 'active' : '' ?>" href="<?= BASEPAGES ?>add-customers.php">Add Customers</a>
                <a class="collapse-item <?= $sidebarSubSection == 'manage-customers' ? 'active' : '' ?>" href="<?= BASEPAGES ?>manage-customers.php">Manage Customers</a>
            </div>
        </div>
    </li>

    <li class="nav-item <?= $sidebarSection == 'Transaction' ? 'active' : '' ?>">
        <a class="nav-link <?= $sidebarSection == 'Transaction' ? '' : 'collapsed' ?>" href="#" data-toggle="collapse" data-target="#collapseTransaction" aria-expanded="true" aria-controls="collapseTransaction">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Transaction</span>
        </a>
        <div id="collapseTransaction" class="collapse <?= $sidebarSection == 'Transaction' ? 'show' : '' ?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item <?= $sidebarSubSection == 'add-purchase' ? 'active' : '' ?>" href="<?= BASEPAGES ?>add-purchase.php">purchase</a>
                <a class="collapse-item <?= $sidebarSubSection == 'add-sales' ? 'active' : '' ?>" href="<?= BASEPAGES ?>add-sales.php">Sale</a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Addons
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-folder"></i>
            <span>Pages</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Login Screens:</h6>
                <a class="collapse-item" href="login.html">Login</a>
                <a class="collapse-item" href="register.html">Register</a>
                <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
                <div class="collapse-divider"></div>
                <h6 class="collapse-header">Other Pages:</h6>
                <a class="collapse-item" href="404.html">404 Page</a>
                <a class="collapse-item" href="blank.html">Blank Page</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Charts -->
    <li class="nav-item">
        <a class="nav-link" href="charts.html">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Charts</span></a>
    </li>

    <!-- Nav Item - Tables -->
    <li class="nav-item">
        <a class="nav-link" href="tables.html">
            <i class="fas fa-fw fa-table"></i>
            <span>Tables</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>