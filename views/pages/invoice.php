<?php

require_once __DIR__ . "/../../helper/init.php";

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    Util::dd($di->get('sales')->getDetailsById($id));
}
