<?php
require_once __DIR__ . "../../../helper/init.php";
Util::createCSRFToken();

$data = '';
$errors = '';

if (Session::hasSession('old')) {
    $data = Session::getSession('old');
    Session::unsetSession('old');
}

if (Session::hasSession('errors')) {
    $errors = unserialize(Session::getSession('errors'));
    Session::unsetSession('errors');
}

if (isset($_GET['id']) && !empty($_GET['id'])) {
    $id = $_GET['id'];
    $result = $di->get('database')->exists('customers', ['id' => $id]);
    if ($result) {
        $data = $di->get('database')->readData('customers', [], "id = ${id}", PDO::FETCH_ASSOC)[0];
    } else {
        Util::redirect('manage-customers.php');
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    require_once __DIR__ . "../../includes/head-section.php";
    ?>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php
        require_once __DIR__ . "../../includes/sidebar.php";
        ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php
                require_once __DIR__ . "../../includes/navbar.php";
                ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card shadow mb-4">
                                <!-- CARD HEADER -->
                                <div class="card-header">
                                    <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-plus"></i>Add Customers</h6>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form action="<?= BASEURL ?>helper/routing.php" method="POST" id="add-customers">
                            <input type="hidden" name="csrf_token" value="<?= Session::hasSession('csrf_token') == 'true' ? Session::getSession('csrf_token') : '' ?>">
                            <input type="hidden" name="id" value="<?= $data != '' ? $data['id'] : '' ?>">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="first_name">First Name</label>
                                        <input type="text" name="first_name" value="<?= $data != '' ? $data['first_name'] : '' ?>" class="form-control <?= $errors != '' ? ($errors->has('first_name') ? 'error is-invalid' : '') : '' ?>">
                                        <?php
                                        if ($errors != '' && $errors->has('first_name')) :
                                            echo "<span class='error'>{$errors->first('first_name')}<span>";
                                        endif;
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="last_name">Last Name</label>
                                        <input type="text" name="last_name" id="" placeholder="Last Name" class="form-control  <?= $errors != '' ? ($errors->has('last_name') ? 'error is-invalid' : '') : '' ?>" value="<?= $data != '' ? $data['last_name'] : '' ?>">
                                        <?php
                                        if ($errors != '' && $errors->has('last_name')) :
                                            echo "<span class='error'>{$errors->first('last_name')}<span>";
                                        endif;
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="gst_no">GST No.</label>
                                        <input type="text" name="gst_no" id="" class="form-control  <?= $errors != '' ? ($errors->has('gst_no') ? 'error is-invalid' : '') : '' ?>" placeholder="GST No" value="<?= $data != '' ? $data['gst_no'] : '' ?>">
                                        <?php
                                        if ($errors != '' && $errors->has('gst_no')) :
                                            echo "<span class='error'>{$errors->first('gst_no')}<span>";
                                        endif;
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone_no">Phone No</label>
                                        <input type="text" name="phone_no" id="" class="form-control  <?= $errors != '' ? ($errors->has('phone_no') ? 'error is-invalid' : '') : '' ?>" placeholder="Phone No" value="<?= $data != '' ? $data['phone_no'] : '' ?>">
                                        <?php
                                        if ($errors != '' && $errors->has('phone_no')) :
                                            echo "<span class='error'>{$errors->first('phone_no')}<span>";
                                        endif;
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" name="email" id="" class="form-control  <?= $errors != '' ? ($errors->has('email') ? 'error is-invalid' : '') : '' ?>" placeholder="Email" value="<?= $data != '' ? $data['email'] : '' ?>">
                                        <?php
                                        if ($errors != '' && $errors->has('email')) :
                                            echo "<span class='error'>{$errors->first('email')}<span>";
                                        endif;
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <div>
                                            <label for="gender">Gender : </label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="male" name="gender" checked value="male" <?= $data != '' ? ($data['gender'] == 'male' ? 'checked' : '') : '' ?> class="custom-control-input">
                                            <label class="custom-control-label" for="male">Male</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="female" name="gender" value="female" <?= $data != '' ? ($data['gender'] == 'female' ? 'checked' : '') : '' ?> class="custom-control-input">
                                            <label class="custom-control-label" for="female">Female</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="other" name="gender" <?= $data != '' ? ($data['gender'] == 'other' ? 'checked' : '') : '' ?> value="other" class="custom-control-input">
                                            <label class="custom-control-label" for="other">Other</label>
                                        </div>
                                    </div>
                                    <?php
                                    if ($errors != '' && $errors->has('gender')) :
                                        echo "<span class='error'>{$errors->first('gender')}<span>";
                                    endif;
                                    ?>
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary" name="edit_customer" value="submit">
                        </form>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php
            require_once __DIR__ . "../../includes/footer.php";
            ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php
    require_once __DIR__ . "../../includes/scroll-to-top.php";
    ?>
    <!-- End of Scroll to Top Button-->

    <?php
    require_once __DIR__ . "../../includes/core-scripts.php";
    ?>

    <script src="<?= BASEASSETS; ?>/js/plugins/jquery-validation/jquery.validation.min.js"></script>
    <!-- <script src="<?= BASEASSETS; ?>/js/pages/customers/add-customers.js"></script> -->

</body>

</html>