<?php
require_once __DIR__ . "/../../helper/init.php";
$page_title = "Quick ERP | Add New Product";
$sidebarSection = "product";
$sidebarSubSection = "add";
Util::createCSRFToken();
$errors = "";
$old = "";
$hsn_codes = $di->get('database')->readData('gst', ['id', 'hsn_code'], 'deleted=0');


if (Session::hasSession('old')) {
    $old = Session::getSession('old');
    Session::unsetSession('old');
}

if (Session::hasSession('errors')) {
    $errors = unserialize(Session::getSession('errors'));
    Session::unsetSession('errors');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    require_once __DIR__ . "../../includes/head-section.php";
    ?>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php
        require_once __DIR__ . "../../includes/sidebar.php";
        ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php
                require_once __DIR__ . "../../includes/navbar.php";
                ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card shadow mb-4">
                                <!-- CARD HEADER -->
                                <div class="card-header">
                                    <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-plus"></i>Add Product</h6>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form action="<?= BASEURL ?>helper/routing.php" method="POST" id="add-customers">
                            <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token'); ?>">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Product Name</label>
                                        <input type="text" name="name" id="name" class="form-control" placeholder="Enter Product Name">
                                        <?php
                                        if ($errors != '' && $errors->has('name')) :
                                            echo "<span class='error'>{$errors->first('name')}</span>";
                                        endif;
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="specification">Specification</label>
                                        <input type="text" name="specification" id="specification" class="form-control" placeholder="Enter Product Specification" value="<?= $old != '' ? $old['specification'] : '' ?>">
                                        <?php
                                        if ($errors != '' && $errors->has('specification')) :
                                            echo "<span class='error'>{$errors->first('specification')}</span>";
                                        endif;
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="hsn_code">HSN CODE</label>
                                        <select name="hsn_code" id="hsn_code" class="form-control">
                                            <?php
                                            $hsn_codes = $di->get('database')->readData('gst', ['id', 'hsn_code'], 'deleted=0');
                                            // Util::dd($hsn_codes);
                                            foreach ($hsn_codes as $row) {
                                                echo "<option value='{$row->hsn_code}'>{$row->hsn_code}</option>";
                                            }
                                            ?>
                                        </select>
                                        <?php
                                        if ($errors != '' && $errors->has('hsn_code')) :
                                            echo "<span class='error'>{$errors->first('hsn_code')}</span>";
                                        endif;
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="supplier_id">Suppliers</label>
                                        <select name="supplier_id[]" id="supplier_id" class="form-control" multiple>
                                            <?php
                                            $suppliers = $di->get('database')->readData('suppliers', ['id', 'first_name', 'last_name'], 'deleted=0');
                                            foreach ($suppliers as $supplier) {
                                                echo "<option value={$supplier->id}>{$supplier->first_name} {$supplier->last_name}</option>";
                                            }
                                            ?>
                                        </select>
                                        <?php
                                        if ($errors != '' && $errors->has('supplier_id')) :
                                            echo "<span class='error'>{$errors->first('supplier_id')}</span>";
                                        endif;
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="category">Category</label>
                                        <select name="category_id" id="category_id" class="form-control">
                                            <?php
                                            $categories = $di->get('database')->readData('category', ['id', 'name'], 'deleted=0');
                                            foreach ($categories as $category) {
                                                echo "<option value={$category->id}>{$category->name}</option>";
                                            }
                                            ?>
                                        </select>
                                        <?php
                                        if ($errors != '' && $errors->has('category_id')) :
                                            echo "<span class='error'>{$errors->first('category_id')}</span>";
                                        endif;
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="selling_rate">Selling Price</label>
                                        <input type="text" name="selling_rate" id="selling_rate" class="form_control" placeholder="Enter Product Selling Price">
                                        <?php
                                        if ($errors != '' && $errors->has('selling_rate')) :
                                            echo "<span class='error'>{$errors->first('selling_rate')}</span>";
                                        endif;
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="eoq_level">EOQ Level</label>
                                        <input type="text" name="eoq_level" id="eoq_level" class="form_control" placeholder="Enter product EOQ Level">
                                        <?php
                                        if ($errors != '' && $errors->has('eoq_level')) :
                                            echo "<span class='error'>{$errors->first('eoq_level')}</span>";
                                        endif;
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="danger_level">Danger Level</label>
                                        <input type="text" name="danger_level" id="danger_level" class="form_control" placeholder="Enter product Danger Level">
                                        <?php
                                        if ($errors != '' && $errors->has('danger_level')) :
                                            echo "<span class='error'>{$errors->first('danger_level')}</span>";
                                        endif;
                                        ?>
                                    </div>
                                </div>


                            </div>
                            <input type="submit" class="btn btn-primary" name="add_product" value="submit">
                        </form>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php
            require_once __DIR__ . "../../includes/footer.php";
            ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php
    require_once __DIR__ . "../../includes/scroll-to-top.php";
    ?>
    <!-- End of Scroll to Top Button-->

    <?php
    require_once __DIR__ . "../../includes/core-scripts.php";
    ?>

    <script src="<?= BASEASSETS; ?>/js/plugins/jquery-validation/jquery.validation.min.js"></script>
    <script src="<?= BASEASSETS; ?>/js/pages/customers/add-product.js"></script>

</body>

</html>