<?php
require_once __DIR__ . "../../../helper/init.php";

$sidebarSection = "Transaction";
$sidebarSubSection = "add-sales";
$pageTitle = "Quick ERP | Dashboard";
Util::createCSRFToken();
$errors = "";
$old = "";
if (Session::hasSession('old')) {
    // these sessions are known as flash sessions which are only valid for one request.
    $old = Session::getSession('old');
    Session::unsetSession('old');
}
if (Session::hasSession('errors')) {
    $errors = unserialize(Session::getSession('errors'));
    Session::unsetSession('errors');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    require_once __DIR__ . "../../includes/head-section.php";
    ?>
    <style>
        .email-verify {
            background-color: green;
            padding: 5px 10px;
            color: white;
            font-size: .875rem;
            line-height: 1.5;
            border-radius: .2rem;
            vertical-align: middle;
            /* display: none !important; */
        }
    </style>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php
        require_once __DIR__ . "../../includes/sidebar.php";
        ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php
                require_once __DIR__ . "../../includes/navbar.php";
                ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <div class="d-sm-flex align-items-center justify-content-between">
                        <h3 class="h3 mb-4 text-gray-800">Sales</h3>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card shadow">

                                <!-- CARD HEADER -->
                                <div class="card-header py-3 d-flex align-items-center justify-content-end ">
                                    <div class="mr-3">
                                        <input type="text" name="email" id="customer_email" class="form-control" placeholder="Enter Email of Customer">
                                    </div>
                                    <div class="">
                                        <p class="email-verify d-none mb-0" id="email_verify_success">
                                            <i class="fas fa-check fa-sm text-white mr-1"></i> Email Verified
                                        </p>
                                        <p class="email-verify bg-danger d-none mb-0" id="email_verify_fail">
                                            <i class="fas fa-times fa-sm text-white mr-1"></i> Email Not Verified
                                        </p>
                                        <a href="<?= BASEPAGES; ?>add-customer.php" class="btn btn-sm btn-warning shadow-sm d-none" id="add_customer_btn">
                                            <i class="fas fa-users fa-sm text-white"></i> Add Customer
                                        </a>
                                        <button type="button" class="d-sm-inline-block btn btn-sm btn-primary  shadow-sm" id="check_email_btn">
                                            <i class="fas fa-envelope fa-sm text-white"></i> Check Email
                                        </button>
                                    </div>
                                </div>
                                <div class="card-header py-3 d-flex align-items-center justify-content-between ">
                                    <h6 class="m-9 font-weight-bold text-primary">
                                        <i class="fas fa-plus"></i>Sales
                                    </h6>
                                    <button type="button" onclick="addProduct();" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                                        <i class="fas fa-plus fa-sm text-white"></i> Add one more product
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <form action="<?= BASEURL ?>helper/routing.php" method="POST" id="add_sales">
                        <input type="hidden" name="customer_id" id="customer_id" value="0">
                        <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token'); ?>">
                        <div class="card-body ">
                            <div id="products_container">
                                <!-- ONE PRODUCT ROW -->
                                <div class="row product_row" id="element_1">
                                    <!-- CATEGORY SELECT -->
                                    <div class="col-md-2 ">
                                        <div class="form-group ">
                                            <label for="">Category</label>
                                            <select name="category_id[]" class="form-control category_select" id="category_1">
                                                <option disabled selected value="">Select Category</option>
                                                <?php
                                                $categories = $di->get('database')->readData('category', ['id', 'name'], "deleted=0");
                                                foreach ($categories as $category) {
                                                    echo "<option value={$category->id}>{$category->name}</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- END OF CATEGORY SELECT  -->
                                    <!-- PRODUCT SELECT -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Products</label>
                                            <select name="product_id[]" class="form-control product_select" id="products_1">
                                                <option disabled selected value="">Select Products</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- END OF PRODUCT SELECT -->
                                    <!-- Selling price -->

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">Selling Price</label>
                                            <input type="text" name="selling_rate[]" id="selling_rate_1" class="form-control selling_rate" readonly>
                                        </div>
                                    </div>
                                    <!-- End of Selling price -->
                                    <!-- Quantity -->
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="">Quantity</label>
                                            <input type="number" value="0" name="quantity[]" id="quantity_1" class="form-control quantity">
                                        </div>
                                    </div>
                                    <!-- End of  Quantity -->
                                    <!-- Discount -->
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="">Discount</label>
                                            <input type="number" value="0" name="discount[]" id="discount_1" class="form-control discount">
                                        </div>
                                    </div>
                                    <!-- End of Discount -->
                                    <!-- Final Price -->
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">Final Price</label>
                                            <input type="text" name="final_price[]" id="final_price_1" value="0" class="form-control product_final_price" readonly>
                                        </div>
                                    </div>
                                    <!-- End of Final price -->
                                    <!-- Delete Button -->
                                    <div class="col-md-1" style="margin-top: 30px;">
                                        <button type="button" class="btn btn-danger">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </div>
                                    <!-- End of Delete Button -->
                                </div>
                            </div>
                        </div>
                        <div class="card-footer d-flex justify-content-between">
                            <div>
                                <input type="submit" class="btn btn-primary" name="add_sales" value="Submit">
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">Final Total</label>
                                <div class="col-sm-8">
                                    <input type="text" name="final_total" id="final_total" readonly class="form-control">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->

    <!-- Footer -->
    <?php
    require_once __DIR__ . "../../includes/footer.php";
    ?>
    <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php
    require_once __DIR__ . "../../includes/scroll-to-top.php";
    ?>
    <!-- End of Scroll to Top Button-->

    <?php
    require_once __DIR__ . "../../includes/core-scripts.php";
    ?>

    <script src="<?= BASEASSETS; ?>/js/plugins/jquery-validation/jquery.validation.min.js"></script>
    <script src="<?= BASEASSETS; ?>/js/pages/transactions/add-sales.js"></script>

</body>

</html>