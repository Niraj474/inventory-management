<?php
require_once __DIR__ . "../../../helper/init.php";

$sidebarSection = "category";
$sidebarSubSection = "add";
$pageTitle = "Quick ERP | Dashboard";
Util::createCSRFToken();
$errors = "";
$old = "";
if (Session::hasSession('old')) {
    // these sessions are known as flash sessions which are only valid for one request.
    $old = Session::getSession('old');
    Session::unsetSession('old');
}
if (Session::hasSession('errors')) {
    $errors = unserialize(Session::getSession('errors'));
    Session::unsetSession('errors');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    require_once __DIR__ . "../../includes/head-section.php";
    ?>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php
        require_once __DIR__ . "../../includes/sidebar.php";
        ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php
                require_once __DIR__ . "../../includes/navbar.php";
                ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card shadow mb-4">
                                <!-- CARD HEADER -->
                                <div class="card-header">
                                    <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-plus"></i>Add Category</h6>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form action="<?= BASEURL ?>helper/routing.php" method="POST" id="add-category">
                            <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token'); ?>">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name">Category Name</label>
                                        <input type="text" name="name" id="name" placeholder="Enter Category Name" class="form-control <?= $errors != '' ? ($errors->has('name') ? 'error is-invalid' : '') : '' ?>" value="<?= $old != '' ? $old['name'] : '' ?>">
                                        <?php
                                        if ($errors != "" && $errors->has('name')) :
                                            echo "<span class='error'> {$errors->first('name')}</span>";
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary" name="add_category" value="submit">
                        </form>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php
            require_once __DIR__ . "../../includes/footer.php";
            ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php
    require_once __DIR__ . "../../includes/scroll-to-top.php";
    ?>
    <!-- End of Scroll to Top Button-->

    <?php
    require_once __DIR__ . "../../includes/core-scripts.php";
    ?>

    <script src="<?= BASEASSETS; ?>/js/plugins/jquery-validation/jquery.validation.min.js"></script>
    <script src="<?= BASEASSETS; ?>/js/pages/category/add-category.js"></script>

</body>

</html>