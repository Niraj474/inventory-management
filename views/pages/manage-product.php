<?php
require_once __DIR__ . "../../../helper/init.php";
$sidebarSection = "product";
$sidebarSubSection = "manage";
$pageTitle = "Quick ERP | Dashboard";
Util::createCSRFToken();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    require_once __DIR__ . "../../includes/head-section.php";
    ?>
    <link rel="stylesheet" href="<?= BASEASSETS; ?>css/plugins/toastr/toastr.min.css">
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php
        require_once __DIR__ . "../../includes/sidebar.php";
        ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php
                require_once __DIR__ . "../../includes/navbar.php";
                ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <h3 class="h3 mb-4 text-gray-800">Manage Product</h3>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Categories</h6>
                        </div>
                        <div class="card-body">
                            <div id="export-buttons"></div>
                            <table class="table table-bordered table-responsive" id="manage-product-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Product Name</th>
                                        <th>Specifications</th>
                                        <th>Selling Rate</th>
                                        <th>WEF</th>
                                        <th>Category</th>
                                        <th>EOQ</th>
                                        <th>Danger Level</th>
                                        <th>Supplier Names</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
            <!-- EDIT MODAL -->
            <!-- /EDIT MODAL -->
            <!-- DELETE MODAL -->
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="deleteModalLabel">Delete Product</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="<?= BASEURL; ?>helper/routing.php" method="POST" id="deleteForm">
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token'); ?>">
                                    <input type="hidden" name="record_id" id="delete_record_id">
                                    <p class="text-muted">Are you sure you want to delete this record?</p>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="close_modal">Cancel</button>
                                <button type="submit" class="btn btn-danger" name="deleteProduct">Delete Record!</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /DELETE MODAL -->
            <!-- Footer -->
            <?php
            require_once __DIR__ . "../../includes/footer.php";
            ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php
    require_once __DIR__ . "../../includes/scroll-to-top.php";
    ?>
    <!-- End of Scroll to Top Button-->

    <?php
    require_once __DIR__ . "../../includes/core-scripts.php";
    require_once __DIR__ . "../../includes/page-level/product/manage-product-scripts.php"
    ?>

</body>

</html>