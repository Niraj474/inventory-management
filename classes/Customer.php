<?php

class Customer
{
    private $table = "customers";
    private $columns = ['id', 'first_name', 'last_name', 'gst_no', 'phone_no', 'email_id', 'gender'];
    private $database;
    private $di;
    private $validator;
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $di->get('database');
    }

    public function addCustomer($data)
    {
        $this->validateData($data);

        if (!$this->validator->fails()) {
            try {
                $this->database->beginTransaction();
                $data_to_be_inserted = [
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'gst_no' => $data['gst_no'],
                    'phone_no' => $data['phone_no'],
                    'email' => $data['email'],
                    'gender' => $data['gender']
                ];
                $cust_id = $this->database->insert($this->table, $data_to_be_inserted);
                $this->database->commit();
                return ADD_SUCCESS;
            } catch (Exception $e) {
                $this->database->rollBack();
                return ADD_ERROR;
            }
        }
        return VALIDATION_ERROR;
    }

    public function getValidator()
    {
        return $this->validator;
    }

    public function validateData($data, $id = "")
    {
        $this->validator = $this->di->get('validator')->check($data, [
            'first_name' => [
                'minlength' => 3,
                'maxlength' => 20,
                'required' => true
            ],
            'last_name' => [
                'minlength' => 3,
                'maxlength' => 20,
                'required' => true
            ],
            'gst_no' => [
                'minlength' => 5,
                'maxlength' => 10,
                'required' => true,
                'unique' => "$this->table:{$id}"
            ],
            'phone_no' => [
                'minlength' => 7,
                'maxlength' => 10,
                'required' => true,
                'unique' => "$this->table:{$id}"
            ],
            'email' => [
                'email' => $data['email'],
                'required' => true,
                'unique' => "$this->table:${id}"
            ]
        ]);
    }

    public function update($data, $id)
    {
        $this->validateData($data, $id);
        if (!$this->validator->fails()) {

            try {
                $data_to_be_updated = [];
                foreach ($this->columns as $field) {
                    $data_to_be_updated[$field] = $data[$field];
                }
                $id = $data_to_be_updated['id'];
                unset($data_to_be_updated['id']);
                $this->database->beginTransaction();
                $this->database->update($this->table, $data_to_be_updated, "id=${id}");
                $this->database->commit();
                return UPDATE_SUCCESS;
            } catch (Exception $e) {
                $this->database->rollBack();
                return UPDATE_ERROR;
            }
        } else {
            return VALIDATION_ERROR;
        }
    }


    public function getJSONDataForDataTable($draw, $search_parameter, $order_by, $start, $length)
    {
        $query = "SELECT * FROM {$this->table} WHERE deleted = 0";
        $totalRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->table} WHERE deleted = 0";
        $filteredRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->table} WHERE deleted = 0";

        if ($search_parameter != null) {
            $query .= " AND {$this->columns[$order_by[0]['column']]} LIKE '%{$search_parameter}%'";
            $filteredRowCountQuery .= " AND {$this->columns[$order_by[0]['column']]} LIKE '%{$search_parameter}%'";
        }

        if ($order_by != null) {
            $query .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
            $filteredRowCountQuery .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
        } else {
            $query .= " ORDER BY {$this->columns[0]} ASC";
            $filteredRowCountQuery .= " ORDER BY {$this->columns[0]} ASC";
        }
        if ($length != -1) {
            $query .= " LIMIT {$start},{$length}";
        }
        $totalNumberOfRecords = $this->database->raw($totalRowCountQuery);
        $filteredNumberOfRecords = $this->database->raw($filteredRowCountQuery);
        // Util::dd($totalNumberOfRecords);

        $totalNumberOfRecords = is_array($totalNumberOfRecords) ? $totalNumberOfRecords[0]->total_count : 0;
        $filteredNumberOfRecords = is_array($filteredNumberOfRecords) ? $filteredNumberOfRecords[0]->total_count : 0;

        $fetchedData = $this->database->raw($query);
        $data = [];
        $numRows = is_array($fetchedData) ? count($fetchedData) : 0;
        // Util::dd([$numRows, $filteredNumberOfRecords]);
        for ($i = 0; $i < $numRows; $i++) {
            $subArray = [];
            $subArray[] = $start + $i + 1;
            $subArray[] = $fetchedData[$i]->first_name;
            $subArray[] = $fetchedData[$i]->last_name;
            $subArray[] = $fetchedData[$i]->gst_no;
            $subArray[] = $fetchedData[$i]->phone_no;
            $subArray[] = $fetchedData[$i]->email_id;
            $subArray[] = $fetchedData[$i]->gender;
            $subArray[] = <<<BUTTONS
<a href="edit-customers.php/?id={$fetchedData[$i]->id}" class = "btn btn-primary btn-sm" data-id='{$fetchedData[$i]->id}'><i class="fas fa-pencil-alt"></i></a>
<button class = "btn btn-danger btn-sm delete" data-target="#deleteModal" data-toggle="modal" data-id='{$fetchedData[$i]->id}'><i class="fas fa-trash"></i></button>
BUTTONS;

            $data[] = $subArray;
        }

        $output = [
            'draw' => $draw,
            'recordsTotal' => $totalNumberOfRecords,
            'recordsFiltered' => $filteredNumberOfRecords,
            'data' => $data
        ];

        // Util::dd($output);

        echo json_encode($output);
    }

    public function delete($id)
    {
        try {
            $this->database->beginTransaction();
            $this->database->delete($this->table, "id=${id}");
            $this->database->commit();
            return DELETE_SUCCESS;
        } catch (Exception $e) {
            $this->database->rollBack();
            return DELETE_ERROR;
        }
    }

    public function getCustomerByEmail($email)
    {
        $result = $this->database->readData($this->table, ['id', 'email_id'], "email_id='$email'");
        return count($result) ? $result[0] : [];
    }
}
