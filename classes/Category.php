<?php

class Category
{
    private $table = "category";
    private $columns = ['id', 'name'];
    protected $di;
    private $database;
    private $validator;
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    public function getValidator()
    {
        return $this->validator;
    }
    public function validateData($data, $id = "")
    {
        $this->validator = $this->di->get('validator');
        $this->validator = $this->validator->check($data, [
            'name' => [
                'required' => true,
                'minlength' => 3,
                'maxlength' => 20,
                'unique' => "$this->table:$id"
            ],
            'category_name' => [
                'required' => true,
                'minlength' => 3,
                'maxlength' => 20,
                'unique' => "$this->table:$id"
            ]
        ]);
    }
    public function addCategory($data)
    {
        // we have to validate the data first
        $this->validateData($data);

        if (!$this->validator->fails()) {
            try {
                $this->database->beginTransaction();
                // database related operations should be written between beginTransaction and commit and all those operations will be executed on commit so that if any failure occurs we should be able to rollback the transcation so that it should not be in an inconsistent state.
                $data_to_be_inserted = ['name' => $data['name']];
                $category_id = $this->database->insert($this->table, $data_to_be_inserted);
                $this->database->commit();
                return ADD_SUCCESS;
            } catch (Exception $e) {
                $this->database->rollBack();
                return ADD_ERROR;
            }
        }
        return VALIDATION_ERROR;
    }

    public function getJSONDataForDataTable($draw, $search_parameter, $order_by, $start, $length)
    {
        $query = "SELECT * FROM {$this->table} WHERE deleted = 0";
        $totalRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->table} WHERE deleted = 0";
        $filteredRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->table} WHERE deleted = 0";

        if ($search_parameter != null) {
            $query .= " AND name LIKE '%{$search_parameter}%'";
            $filteredRowCountQuery .= " AND name LIKE '%{$search_parameter}%'";
        }
        // Util::dd($this->columns[$order_by[0]['column']]);
        if ($order_by != null) {
            $query .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
            $filteredRowCountQuery .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
        } else {
            $query .= " ORDER BY {$this->columns[0]} ASC";
            $filteredRowCountQuery .= " ORDER BY {$this->columns[0]} ASC";
        }

        if ($length != -1) {
            $query .= " LIMIT ${start},${length}";
        }

        $totalRowCountResult = $this->database->raw($totalRowCountQuery);
        $numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count : 0;
        $filteredRowCountResult = $this->database->raw($filteredRowCountQuery);
        $numberOfFilteredRows = is_array($filteredRowCountResult) ? $filteredRowCountResult[0]->total_count : 0;



        $fetchedData = $this->database->raw($query);
        $data = [];
        $numRows = is_array($fetchedData) ? count($fetchedData) : 0;
        for ($i = 0; $i < $numRows; $i++) {
            $subArray = [];
            $subArray[] = $start + $i + 1;
            $subArray[] = "<span class='category_name'>{$fetchedData[$i]->name}</span>";
            $subArray[] = <<<BUTTONS
<button class = "btn btn-primary btn-sm edit" data-target="#editModal" data-toggle="modal" data-id='{$fetchedData[$i]->id}'><i class="fas fa-pencil-alt"></i></button>
<button class = "btn btn-danger btn-sm delete" data-target="#deleteModal" data-toggle="modal" data-id='{$fetchedData[$i]->id}'><i class="fas fa-trash-alt"></i></button>
BUTTONS;
            $data[] = $subArray;
        }

        $output = array(
            'draw' => $draw,
            'recordsTotal' => $numberOfTotalRows,
            'recordsFiltered' => $numberOfFilteredRows,
            'data' => $data
        );

        echo json_encode($output);
    }

    public function all()
    {
        return $this->database->readData($this->table, [], "deleted=0");
    }

    public function getCategoryById($category_id, $mode)
    {
        $query = "SELECT * FROM {$this->table} WHERE id = ${category_id} AND deleted = 0";
        $fetchedData = $this->database->raw($query, $mode);
        return $fetchedData;
    }

    public function update($data, $id)
    {
        $data_to_be_updated = ['name' => $data['category_name']];
        $this->validateData($data_to_be_updated, $id);
        if (!$this->validator->fails()) {
            try {
                $this->database->beginTransaction();
                $this->database->update($this->table, $data_to_be_updated, "id = {$id}");
                $this->database->commit();
                return UPDATE_SUCCESS;
            } catch (Exception $e) {
                $this->database->rollBack();
                return UPDATE_ERROR;
            }
        }
        return VALIDATION_ERROR;
    }

    public function delete($id)
    {
        try {
            $this->database->beginTransaction();
            $this->database->delete($this->table, "id=${id}");
            $this->database->commit();
            return DELETE_SUCCESS;
        } catch (Exception $e) {
            $this->database->rollBack();
            return DELETE_ERROR;
        }
    }
}
