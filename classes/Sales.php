<?php

class Sales
{
    private $table = "sales";
    private $database;

    public function __construct(DependencyInjector $di)
    {
        $this->database = $di->get('database');
    }

    public function getDetailsById($id)
    {
        $query = "SELECT * FROM (SELECT invoice.id as inv_id,customers.first_name,customers.last_name,customers.phone_no,customers.gst_no,customers.email_id,sales.invoice_id,sales.product_id,sales.quantity,sales.discount,sales.created_at,MAX(products_selling_rate.with_effect_from) as wef FROM `invoice` INNER JOIN customers ON invoice.customer_id = customers.id INNER JOIN sales ON invoice.id=sales.invoice_id INNER JOIN products_selling_rate ON products_selling_rate.product_id = sales.product_id AND sales.created_at > products_selling_rate.with_effect_from WHERE invoice.id = $id GROUP BY products_selling_rate.product_id) as res INNER JOIN products_selling_rate ON res.product_id = products_selling_rate.product_id AND res.wef = products_selling_rate.with_effect_from";

        return $this->database->raw($query);
    }

    public function createSales($data)
    {
        $ok = false;
        if ($data['customer_id']) {
            $cust_id = $data['customer_id'];
            $customerData = ['customer_id' => $cust_id];

            try {
                $this->database->beginTransaction();
                $invoiceId = $this->database->insert('invoice', $customerData);
                if (isset($data['product_id'])) {
                    $sales = [];
                    $products = $data['product_id'];
                    $quantities = $data['quantity'];
                    $discount = $data['discount'];
                    for ($i = 0; $i < count($products); $i++) {
                        $sales['product_id'] = $products[$i];
                        if ($quantities[$i]) {
                            $sales['quantity'] = $quantities[$i];
                            $ok = true;
                        }
                        $sales['discount'] = $discount[$i];
                        $sales['invoice_id'] = $invoiceId;
                        if ($ok) {
                            $this->database->insert('sales', $sales);
                        }
                    }
                }
                $this->database->commit();
                return $invoiceId;
            } catch (Exception $e) {
                $this->database->rollBack();
                $e->getMessage();
                return ADD_ERROR;
            }
        } else {
            return VERIFICATION_ERROR;
        }
    }
}
