<?php

class Hash
{
    public function make($plainText)
    {
        return password_hash($plainText, PASSWORD_BCRYPT, ["cost" => 10]); //cost 10 means hashing algo must be applied that much no of times.PASSWORD_BCRYPT is the algo.
        // Every time it will generate a random hashed password bcoz of random salt.
    }
    public function verify($plain, $hash)
    {
        return password_verify($plain, $hash); //used to test plain text password provided while signing in with the hashed password which is stored in database.
    }
    public function generateRandomToken($id)
    {
        return hash("sha256", $id . TokenHandler::getCurrentTimeInMilliSec() . strrev($id) . rand());
    }
}
