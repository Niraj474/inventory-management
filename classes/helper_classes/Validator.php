<?php

class Validator
{
    protected $database;
    protected $errorHandler;

    protected $rules = ["required", "minlength", "maxlength", "unique", "email"];
    protected $messages = [
        "required" => "This :field field is required",
        "minlength" => "This :field field must be a minumum of :satisfier characters",
        "maxlength" => "This :field field must be a maximum of :satisfier characters",
        "email" => "This is not a valid email address",
        "unique" => "That :field is already taken"
    ];

    public function __construct(DependencyInjector $di)
    {
        $this->database = $di->get('database');
        $this->errorHandler = $di->get('error_handler');
    }
    public function check($items, $rules)
    {
        foreach ($items as $item => $value) //items is an associative array and $item are keys of $items and $ value are values of $items.
        {
            if (in_array($item, array_keys($rules))) {
                $this->validate([
                    'field' => $item,
                    'value' => $value,
                    'rules' => $rules[$item]
                ]);
            }
        }
        return $this;
    }

    public function fails()
    {
        return $this->errorHandler->hasErrors();
    }

    public function errors()
    {
        return $this->errorHandler;
    }
    private function validate($item)
    {
        /**
         * $item['field'] -> contains the column name which has to be tested for validation
         * $item['value'] -> contains the value which was inserted by the user in the form
         * $item['rules'] -> it is an array of rules to be applied for the specific field
         */

        $field = $item['field'];
        $value = $item['value'];
        foreach ($item['rules'] as $rule => $satisfier) {
            if (!call_user_func_array([$this, $rule], [$field, $value, $satisfier])) {
                //store the error in the error handler
                $this->errorHandler->addError(str_replace([':field', ':satisfier'], [$field, $satisfier], $this->messages[$rule]), $field);
            }
        }
    }

    private function required($field, $value, $satisfier)
    {
        return !empty(trim($value));
    }
    private function minLength($field, $value, $satisfier)
    {
        return mb_strlen($value) >= $satisfier;
    }
    private function maxLength($field, $value, $satisfier)
    {
        return mb_strlen($value) <= $satisfier;
    }
    private function unique($field, $value, $satisfier)
    {
        /**
         * Here satisfier will become the name of the table!
         * $field will become the name of the column
         * $value should be unique under the above table and column
         */
        // Util::dd($satisfier);
        $satisfiers = explode(":", $satisfier);
        $table = $satisfiers[0];
        $id = $satisfiers[1];
        // Util::dd($satisfiers);
        return !$this->database->exists($table, [$field => $value], $id);
    }
    private function email($field, $value, $satisfier)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }
}
