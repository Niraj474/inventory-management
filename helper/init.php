<?php
ini_set('display_errors', 'on'); //php consists on built in web server bcoz of which we can run php files without apache so that web server consists of ini file and there is an option known as display_errors in ini file which is by default off(i.e.we will not get errors if our script fails in some case).so ini_set is used to change the config of web server for only this project locally.
error_reporting(E_ALL); //this function is used so that we should halt the program for every warning , notice and errors.

date_default_timezone_set("Asia/Kolkata");

session_start();

require_once __DIR__ . "/requirements.php";

$di = new DependencyInjector();
$di->set('config', new Config());
$di->set('database', new Database($di));
$di->set('hash', new Hash());
$di->set('util', new Util($di));
$di->set('error_handler', new ErrorHandler());
$di->set('validator', new Validator($di));

$di->set('category', new Category($di));
$di->set('customers', new Customer($di));
$di->set('product', new Product($di));
$di->set('sales', new Sales($di));

require_once __DIR__ . "/constants.php";
