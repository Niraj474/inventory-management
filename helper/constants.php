<?php

define('BASEURL', $di->get('config')->get('base_url'));
define('BASEASSETS', BASEURL . "assets/");
define('BASEPAGES', BASEURL . "views/pages/");
define("ADD_SUCCESS", "add success");
define("ADD_ERROR", "add error");
define("UPDATE_SUCCESS", "update success");
define("UPDATE_ERROR", "update error");
define("VALIDATION_ERROR", "validation error");
define("DELETE_ERROR", "Error while deleting");
define("DELETE_SUCCESS", "Delete Success");
define("VERIFICATION_ERROR", "Customer Not Verified");
