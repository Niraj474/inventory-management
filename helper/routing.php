<?php

require_once 'init.php';
if (isset($_POST['add_category'])) {
    if (Util::verifyCSRFToken($_POST)) {
        $result = $di->get('category')->addCategory($_POST);

        switch ($result) {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR, "Add Category Error!");
                Util::redirect("manage-category.php");
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS, "Add Category Success!");
                Util::redirect("manage-category.php");
                break;
            case VALIDATION_ERROR:
                Session::setSession('validation', 'Validation Error');
                Session::setSession('old', $_POST);
                Session::setSession('errors', serialize($di->get('category')->getValidator()->errors()));
                Util::redirect("add-category.php");
                break;
        }
    } else {
        Session::setSession("csrf", "CSRF Error");
        Util::redirect("manage-category.php"); //need to change this, actually we will be redirecting to some error page indicating unauthorized success.
    }
}

if (isset($_POST['add_customer'])) {
    if (Util::verifyCSRFToken($_POST)) {
        // Util::dd($_POST);
        $result = $di->get('customers')->addCustomer($_POST);
        // Util::dd($di->get('customers')->getValidator()->errors());
        switch ($result) {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR, "Add Customer Error!");
                Util::redirect("manage-customers.php");
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS, "Add Customer Success!");
                Util::redirect("manage-customers.php");
                break;
            case VALIDATION_ERROR:
                Session::setSession('validation', 'Validation Error');
                Session::setSession('old', $_POST);
                Session::setSession('errors', serialize($di->get('customers')->getValidator()->errors()));
                Util::redirect("add-customers.php");
                break;
        }
    } else {
        Session::setSession("csrf", "CSRF Error");
        Util::redirect("manage-customers.php");
    }
}
if (isset($_POST['add_product'])) {
    if (Util::verifyCSRFToken($_POST)) {
        Util::dd($_POST);
        $result = $di->get('product')->addProduct($_POST);
        // Util::dd($di->get('customers')->getValidator()->errors());
        switch ($result) {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR, "Add Product Error!");
                Util::redirect("manage-product.php");
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS, "Add Product Success!");
                Util::redirect("manage-product.php");
                break;
            case VALIDATION_ERROR:
                Session::setSession('validation', 'Validation Error');
                Session::setSession('old', $_POST);
                Session::setSession('errors', serialize($di->get('product')->getValidator()->errors()));
                Util::redirect("add-product.php");
                break;
        }
    } else {
        Session::setSession("csrf", "CSRF Error");
        Util::redirect("manage-product.php");
    }
}

if (isset($_POST['page'])) {
    // $_POST['search']['value']
    // $_POST['start']
    // $_POST['length']
    // $_POST['draw']
    // Util::dd($_POST);
    $page = $_POST['page'];
    $dependency = explode('_', $page)[1];
    // Util::dd($page);
    // if ($_POST['page'] == 'manage_category') {
    //     $dependency = 'category';
    // } else {
    //     $dependency = 'customers';
    // }
    $search_parameter = $_POST['search']['value'] ?? null;
    $order_by = $_POST['order'] ?? null;
    $draw = $_POST['draw'];
    $length = $_POST['length'];
    $start = $_POST['start'];
    $di->get($dependency)->getJSONDataForDataTable($draw, $search_parameter, $order_by, $start, $length);
}


if (isset($_POST['fetch'])) {
    if ($_POST['fetch'] == 'category') {
        $category_id = $_POST['category_id'];
        $result = $di->get('category')->getCategoryById($category_id, PDO::FETCH_ASSOC);
        echo json_encode($result[0]);
    }
}

if (isset($_POST['editCategory'])) {
    if (Util::verifyCSRFToken($_POST)) {
        $category_id = $_POST['category_id'];
        $result = $di->get('category')->update($_POST, $category_id);

        switch ($result) {
            case UPDATE_ERROR:
                Session::setSession(ADD_ERROR, "Update Category Error!");
                Util::redirect("manage-category.php");
                break;
            case UPDATE_SUCCESS:
                Session::setSession(ADD_SUCCESS, "Update Category Success!");
                Util::redirect("manage-category.php");
                break;
            case VALIDATION_ERROR:
                Session::setSession('validation', 'Validation Error');
                Session::setSession('errors', serialize($di->get('category')->getValidator()->errors()));
                Util::redirect("manage-category.php");
                break;
        }
    }
}

if (isset($_POST['deleteCategory'])) {
    if (Util::verifyCSRFToken($_POST)) {
        $id = $_POST['record_id'];
        $result = $di->get('category')->delete($id);
        // Util::dd($result);
        switch ($result) {
            case DELETE_ERROR:
                Session::setSession(DELETE_ERROR, "Delete Category Error!");
                Util::redirect("manage-category.php");
                break;
            case DELETE_SUCCESS:
                Session::setSession(DELETE_SUCCESS, "Deleted Successfully!");
                Util::redirect("manage-category.php");
                break;
        }
    }
}
if (isset($_POST['deleteCustomer'])) {
    if (Util::verifyCSRFToken($_POST)) {
        $id = $_POST['record_id'];
        $result = $di->get('customers')->delete($id);
        // Util::dd($result);
        switch ($result) {
            case DELETE_ERROR:
                Session::setSession(DELETE_ERROR, "Delete Customer Error!");
                Util::redirect("manage-customers.php");
                break;
            case DELETE_SUCCESS:
                Session::setSession(DELETE_SUCCESS, "Deleted Successfully!");
                Util::redirect("manage-customers.php");
                break;
        }
    }
}

if (isset($_POST['edit_customer'])) {
    if (Util::verifyCSRFToken($_POST)) {
        $cust_id = $_POST['id'];
        $result = $di->get('customers')->update($_POST, $cust_id);
        // Util::dd($result);
        switch ($result) {
            case UPDATE_ERROR:
                Session::setSession(UPDATE_ERROR, "Update Category Error!");
                Util::redirect("manage-customers.php");
                break;
            case UPDATE_SUCCESS:
                Session::setSession(UPDATE_SUCCESS, "Update Category Success!");
                Util::redirect("manage-customers.php");
                break;
            case VALIDATION_ERROR:
                Session::setSession('validation', 'Validation Error');
                Session::setSession('old', $_POST);

                Session::setSession('errors', serialize($di->get('customers')->getValidator()->errors()));
                Util::redirect("edit-customers.php");
                break;
        }
    }
}

if (isset($_POST['getCategories'])) {
    echo json_encode($di->get('category')->all());
}

if (isset($_POST['getProductsByCategoryId'])) {
    $category_id = $_POST['categoryId'];
    $result = $di->get('product')->getProductsByCategoryId($category_id);

    echo json_encode($result);
}
if (isset($_POST['getSellingPriceByProductId'])) {
    $product_id = $_POST['productId'];
    $result = $di->get('product')->getSellingPriceByProductId($product_id);

    $selling_rate = (int) $result[0]->selling_rate ?? 0;
    echo $selling_rate;
}

if (isset($_POST['getCustomerByEmail'])) {
    $email = $_POST['email'];
    $result = $di->get('customers')->getCustomerByEmail($email);
    echo json_encode($result);
}

if (isset($_POST['add_sales'])) {
    $result = $di->get('sales')->createSales($_POST);

    switch ($result) {
        case VERIFICATION_ERROR: {
                Util::dd(VERIFICATION_ERROR);
                break;
            }
        case ADD_ERROR: {
                Util::dd(ADD_ERROR);
                break;
            }
        default: {
                Util::redirect("invoice.php?id=$result");
            }
    }
}
