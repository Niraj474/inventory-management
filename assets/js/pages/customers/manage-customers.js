var handleCustomers = function () {
    var manageCustomers = function () {
        var manageCustomersTable = $('#manage-customers-table');
        var baseURL = window.location.origin;
        var filePath = "/helper/routing.php";
        var oTable = manageCustomersTable.DataTable({
            'serverSide': true,
            'processing': true,
            'ajax': {
                url: baseURL + filePath,
                method: 'POST',
                data: {
                    'page': 'manage_customers'
                }
            },
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],
            "order": [
                [1, "desc"]
            ],
            "columnDefs": [{
                orderable: false,
                targets: [0]
            }]
        });
        manageCustomersTable.on("click", ".delete", function (e) {
            var id = $(this).data("id");
            $("#delete_record_id").val(id);
        });
        new $.fn.dataTable.Buttons(oTable, {
            buttons: [
                'copy', 'csv', 'pdf'
            ]
        });
        oTable.buttons().container().appendTo($('#export-buttons'));

    }

    return {
        init: function () {
            manageCustomers();
        }
    }
}();



jQuery(document).ready(function () {
    handleCustomers.init();
});