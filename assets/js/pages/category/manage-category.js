    var TableDatatables = (function () {
  var value = "";
  var handleCategoryTable = function () {
    var manageCategoryTable = $("#manage-category-table");
    // var form = $("#editForm");
    // form.submit(function (e) {
    //   if (value === $("#edit_category_name").val()) {
    //     e.preventDefault();
    //     $("#close_modal").click();
    //     return false;
    //   }
    // });
    $('#editForm').validate({
      rules: {
        'category_name': {
          required: true
        }
      },
      submitHandler: function (form) {
        form.submit();
      }
    })
    var baseURL = window.location.origin;
    var filePath = "/helper/routing.php";
    var oTable = manageCategoryTable.DataTable({
      processing: true,
      serverSide: true,
      ajax: {
        url: baseURL + filePath,
        type: "POST",
        data: {
          page: "manage_category",
        },
      },
      lengthMenu: [
        [5, 15, 25, -1], //these are internal values of below array according to index. i.e. if we click on 'All' on UI  then -1 is sent to routing.php
        [5, 15, 25, "All"], //these are shown on UI part
      ],
      order: [
        [1, "desc"], //this is the default ordering i.e. fetch records and order it in descending format according to index 1 of our table on UI(i.e. in our case it is category-name)
      ],
      columnDefs: [{
        orderable: false,
        targets: [0, -1], //these are the index of tables which should not be ordered.
      }, ],
    });
    manageCategoryTable.on("click", ".edit", function (e) {
      var id = $(this).data("id");
      $("#edit_category_id").val(id);
      // Fetching the category name through database
      $.ajax({
        url: baseURL + filePath,
        type: "POST",
        data: {
          category_id: id,
          fetch: "category",
        },
        dataType: "json",
        success: function (data) {
          value = data.name;
          $("#edit_category_name").val(data.name);
        },
      });
    });
    manageCategoryTable.on("click", ".delete", function (e) {
      var id = $(this).data("id");
      $("#delete_record_id").val(id);
    });
    new $.fn.dataTable.Buttons(oTable, {
      buttons: ["copy", "csv", "pdf"],
    });
    oTable.buttons().container().appendTo($("#export-buttons"));
  };
  return {
    // function to handle all the datatables
    init: function () {
      handleCategoryTable();
    },
  };
})();
jQuery(document).ready(function () {
  TableDatatables.init();
});

/*Event delegation to get the category name and id through javascript */
// $("#manage-category-table").on('click', e => {
//     const parent = e.target.closest('tr');
//     const name = parent.querySelector('td .category_name').innerText;
//     $("#edit_category_name").val(name);
//     const id = e.target.closest('.edit').dataset.id;
//     $('#edit_category_id').val(id);
// });