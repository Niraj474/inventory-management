let id = 2;

var baseURL = window.location.origin;
var filePath = "/helper/routing.php";

function ajaxCall(data, success, method = "POST", url = baseURL + filePath, datatype = "json") {
    $.ajax({
        url,
        method,
        datatype,
        data,
        success
    });
}

let checkEmailBtn = $('#check_email_btn');
checkEmailBtn.on('click', checkEmail);

function addProduct() {
    $('#products_container').append(
        `<div class="row product_row" id="element_${id}">
         <!-- CATEGORY SELECT -->
         <div class="col-md-2 ">
             <div class="form-group">
                 <label for="">Category</label>
                 <select name="category_id[]" class="form-control category_select" id="category_${id}">
                     <option disabled selected value="">Select Category</option>
                 </select>
             </div>
         </div>
         <!-- END OF CATEGORY SELECT  -->
         <!-- PRODUCT SELECT -->
         <div class="col-md-3">
             <div class="form-group">
                 <label for="">Products</label>
                 <select name="product_id[]" class="form-control product_select" id="products_${id}">
                     <option disabled selected value="">Select Products</option>
                 </select>
             </div>
         </div>
         <!-- END OF PRODUCT SELECT -->
         <!-- Selling price -->

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">Selling Price</label>
                                            <input type="text" name="selling_rate[]" id="selling_rate_${id}" class="form-control selling_rate" readonly>
                                        </div>
                                    </div>
                                    <!-- End of Selling price -->
                                    <!-- Quantity -->
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="">Quantity</label>
                                            <input type="number" value="0" name="quantity[]" id="quantity_${id}" class="form-control quantity">
                                        </div>
                                    </div>
                                    <!-- End of  Quantity -->
                                    <!-- Discount -->
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="">Discount</label>
                                            <input type="number" value="0" name="discount[]" id="discount_${id}" class="form-control discount">
                                        </div>
                                    </div>
                                    <!-- End of Discount -->
                                    <!-- Final Price -->
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="">Final Price</label>
                                            <input type="text" name="final_price[]" id="final_price_1" value="0" class="form-control product_final_price" readonly>
                                        </div>
                                    </div>
                                    <!-- End of Final price -->
                                    <!-- Delete Button -->
                                    <div class="col-md-1" style="margin-top: 30px;">
                                        <button type="button" onclick="deleteProduct(${id})" class="btn btn-danger">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </div>
                                    <!-- End of Delete Button -->
     </div>`
    )

    function success(categories) {
        categories = JSON.parse(categories);
        categories.forEach(category => {
            $(`#category_${id}`).append(`<option value='${category.id}'>${category.name}</option>`);
        });
        id++;
    }

    ajaxCall({
        "getCategories": true
    }, success);
}

function deleteProduct(id) {
    var elements = document.getElementsByClassName('product_row');
    if (elements.length != 1) {
        $(`#element_${id}`).remove();
    }
    calcTotal();
}

$('#products_container').on('change', '.category_select', function () {
    let elementId = $(this).attr('id').split('_')[1]; //id="category_1"->after split->0:category,1:"1"

    // elementId will be used to insert the data in the products dropdown according to id bcoz for category_1 there is products_1,for category_2 -> products_2....

    let categoryId = this.value;

    ajaxCall({
        'getProductsByCategoryId': true,
        'categoryId': categoryId
    }, success);

    function success(products) {
        products = JSON.parse(products);
        $(`#products_${elementId}`).empty();
        products.forEach(product => {

            $(`#products_${elementId}`).append(
                `<option value="${product.id}">${product.name}</option>`
            )
        });
    }
});
$('#products_container').on('change', '.product_select', function () {
    let elementId = $(this).attr('id').split('_')[1]; //id="products_1"->after split->0:products,1:"1"

    // elementId will be used to insert the data in the products dropdown according to id bcoz for category_1 there is products_1,for category_2 -> products_2....

    let productId = this.value;

    ajaxCall({
        'getSellingPriceByProductId': true,
        'productId': productId
    }, success);

    function success(selling_rate) {
        $(`#selling_rate_${elementId}`).val(selling_rate);
        calcTotal();
    }
});
$('#products_container').on('change', '.discount', function () {
    calcTotal();
});
$('#products_container').on('change', '.quantity', function () {
    calcTotal();
});

function calcTotal() {
    let globalTotal = 0;
    let rows = Array.from(document.getElementsByClassName('product_row'));
    rows.forEach(row => {
        let rowTotal = 0;
        let discount = row.querySelector('.discount').value;
        let quantity = row.querySelector('.quantity').value;
        let selling_rate = row.querySelector('.selling_rate').value ? row.querySelector('.selling_rate').value : 0;

        let finalPrice = row.querySelector('.product_final_price');
        rowTotal = (selling_rate * quantity) - ((selling_rate * quantity * discount) / 100);
        finalPrice.value = rowTotal;
        globalTotal += rowTotal;
    });
    $('#final_total').val(globalTotal);
}




function checkEmail() {
    let email = $('#customer_email').val();
    ajaxCall({
        'getCustomerByEmail': true,
        'email': email
    }, success);

    function success(data) {
        data = JSON.parse(data);
        $('#customer_id').val(0);
        if (data.id) {
            const custId = data.id;
            $('#customer_id').val(custId);
            $('#email_verify_fail').addClass('d-none');
            $('#email_verify_fail').removeClass('d-inline-block');
            $('#add_customer_btn').addClass('d-none');
            $('#add_customer_btn').removeClass('d-inline-block');
            $('#email_verify_success').removeClass('d-none');
            $('#email_verify_success').addClass('d-inline-block');
            return true;

        } else {
            $('#email_verify_success').addClass('d-none');
            $('#email_verify_success').removeClass('d-inline-block');
            $('#email_verify_fail').removeClass('d-none');
            $('#email_verify_fail').addClass('d-inline-block');
            $('#add_customer_btn').removeClass('d-none');
            $('#add_customer_btn').addClass('d-inline-block');

            return false;
        }
    }
}